package examen.mpp.web.converter;

import examen.mpp.core.model.BaseEntity;
import examen.mpp.web.dto.BaseDto;

public interface Converter<Model extends BaseEntity<Long>, Dto extends BaseDto>
        extends ConverterGeneric<Model, Dto> {

}